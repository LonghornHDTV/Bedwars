package me.longhornhdtv.utils;

public enum GameState {
	FULL_LOBBY(), LOBBY(), INGAME(), RESTART();
}
