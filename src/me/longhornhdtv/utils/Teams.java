package me.longhornhdtv.utils;

public enum Teams {
	BLACK(),
	DARK_BLUE(),
    DARK_GREEN(),
    DARK_AQUA(),
	DUNKELROT(),
	DUNKELVIOLETT(),
	DUNKELGRAU(),
	GOLD(),
	GRAU(),
	BLAU(),
	GRUEN(),
	AQUA(),
	ROT(),
	HELLVIOLETT(),
	GELB(),
	WEISS();
}
